<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create New Workflow                    Create New Workflow</name>
   <tag></tag>
   <elementGuidId>0bb32186-74f7-4554-936f-39bbf693295e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/wkf/t-Demo/workflows/add?templateWorkflowID=647&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHnc-QAkGRL-OdcImfeNpFzCSQqjgzxQg6-cNeBdbr6I7SuHMc_djgZ3uWY1vmiYgzwmBw9vs2C7C_imQ8VYAx4kGXFhfoENESOlk0-jlHt1yzEET9_5r9D7q6fBZq8KQus</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>99b33ddc-tooltip</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Create New Workflow
                    Create New Workflow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;workflowTemplateActions&quot;)/ul[1]/li[2]/a[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Publish'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Publish'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/wkf/t-Demo/workflows/add?templateWorkflowID=647&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHnc-QAkGRL-OdcImfeNpFzCSQqjgzxQg6-cNeBdbr6I7SuHMc_djgZ3uWY1vmiYgzwmBw9vs2C7C_imQ8VYAx4kGXFhfoENESOlk0-jlHt1yzEET9_5r9D7q6fBZq8KQus')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav[4]/ul/li[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
