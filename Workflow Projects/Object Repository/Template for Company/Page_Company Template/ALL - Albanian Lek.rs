<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ALL - Albanian Lek</name>
   <tag></tag>
   <elementGuidId>8f3a4f81-0782-4497-a274-979654f6bb4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Workflow_CurrencyCode']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>label_CurrencyCode</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Workflow_CurrencyCode</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Workflow.CurrencyCode</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>(None)
AED - UAE Dirham
AFN - Afghani
ALL - Albanian Lek
AMD - Armenian Dram
ARS - Argentine Peso
AUD - Australian Dollar
AZN - Azerbaijanian Manat
BAM - Convertible Marks
BDT - Bangladeshi Taka
BGN - Bulgarian Lev
BHD - Bahraini Dinar
BND - Brunei Dollar
BOB - Boliviano
BRL - Real
BYN - Belarusian Ruble
BZD - Belize Dollar
CAD - Canadian Dollar
CHF - Swiss Franc
CLP - Chilean Peso
CNY - PRC Renminbi
COP - Colombian Peso
CRC - Costa Rican Colon
CSD - Serbian Dinar
CZK - Czech Koruna
DKK - Danish Krone
DOP - Dominican Peso
DZD - Algerian Dinar
EGP - Egyptian Pound
ETB - Ethiopian Birr
EUR - Euro
GBP - UK Pound Sterling
GEL - Lari
GTQ - Guatemalan Quetzal
HKD - Hong Kong Dollar
HNL - Honduran Lempira
HRK - Croatian Kuna
HUF - Hungarian Forint
IDR - Indonesian Rupiah
ILS - Israeli New Shekel
INR - Indian Rupee
IQD - Iraqi Dinar
IRR - Iranian Rial
ISK - Icelandic Krona
JMD - Jamaican Dollar
JOD - Jordanian Dinar
JPY - Japanese Yen
KES - Kenyan Shilling
KGS - som
KHR - Riel
KRW - Korean Won
KWD - Kuwaiti Dinar
KZT - Tenge
LAK - Kip
LBP - Lebanese Pound
LKR - Sri Lanka Rupee
LYD - Libyan Dinar
MAD - Moroccan Dirham
MKD - Macedonian Denar
MNT - Tugrik
MOP - Macao Pataca
MVR - Rufiyaa
MXN - Mexican Peso
MYR - Malaysian Ringgit
NIO - Nigerian Naira
NOK - Norwegian Krone
NPR - Nepalese Rupees
NZD - New Zealand Dollar
OMR - Omani Rial
PAB - Panamanian Balboa
PEN - Peruvian Nuevo Sol
PHP - Philippine Peso
PKR - Pakistan Rupee
PLN - Polish Zloty
PYG - Paraguay Guarani
QAR - Qatari Rial
RON - Romanian Leu
RSD - Serbian Dinar
RUB - Russian Ruble
RWF - Rwandan Franc
SAR - Saudi Riyal
SEK - Swedish Krona
SGD - Singapore Dollar
SYP - Syrian Pound
THB - Thai Baht
TJS - Ruble
TMT - Turkmen manat
TND - Tunisian Dinar
TRY - Turkish Lira
TTD - Trinidad Dollar
TWD - New Taiwan Dollar
UAH - Ukrainian Grivna
USD - US Dollar
UYU - Peso Uruguayo
UZS - Uzbekistan Som
VES - Venezuelan Bolívar
VND - Vietnamese Dong
XOF - XOF Senegal
YER - Yemeni Rial
ZAR - South African Rand
ZWL - Zimbabwe Dollar
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Workflow_CurrencyCode&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Workflow_CurrencyCode']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default Currency'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Description'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
