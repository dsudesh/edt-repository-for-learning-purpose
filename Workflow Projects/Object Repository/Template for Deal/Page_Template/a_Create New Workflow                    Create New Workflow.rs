<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create New Workflow                    Create New Workflow</name>
   <tag></tag>
   <elementGuidId>dcd22c3b-e081-401b-a062-7505733f0723</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/wkf/t-Demo/workflows/add?templateWorkflowID=596&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHndKL67qb245Akay_2eWqoVsgNK5MeQ5ksvoD9QpLWwPFDWEYnrf3l8MxYMUTkdn_LXGbV-5wj8MIlEDMy0A1W-RoNfTIvqD79Qa-BYRr3-sh_75P2MYqbYxVZ8WJakf3E</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>837fec9c-tooltip</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Create New Workflow
                    Create New Workflow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;workflowTemplateActions&quot;)/ul[1]/li[2]/a[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='workflowTemplateActions']/ul/li[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Publish'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Publish'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/wkf/t-Demo/workflows/add?templateWorkflowID=596&amp;returnUrl=CfDJ8Bs-hiSgmi9EoANiHkufOHndKL67qb245Akay_2eWqoVsgNK5MeQ5ksvoD9QpLWwPFDWEYnrf3l8MxYMUTkdn_LXGbV-5wj8MIlEDMy0A1W-RoNfTIvqD79Qa-BYRr3-sh_75P2MYqbYxVZ8WJakf3E')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav[4]/ul/li[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
