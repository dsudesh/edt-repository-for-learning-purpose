import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/All Test Suite')

suiteProperties.put('name', 'All Test Suite')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\skarthik\\Katalon Studio\\Workflow Projects\\Reports\\All Test Suite\\20190805_155920\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/All Test Suite', suiteProperties, [new TestCaseBinding('Test Cases/UI Validation Test Case', 'Test Cases/UI Validation Test Case',  null), new TestCaseBinding('Test Cases/Template ,Task and Workflow Creation', 'Test Cases/Template ,Task and Workflow Creation',  [ 'Assigned' : 'ssingh' , 'Templatename' : 'Triton Temp1' , 'Taskname' : 'Triton Task 1' , 'WorkflowName' : 'Workflow for Triton1' , 'Comments' : 'Welcome' , 'TaskDescription' : 'EDT Task' , 'WorkflowDescription' : 'EDT Tester' , 'TemplateDescription' : 'QA Triton' ,  ])])
