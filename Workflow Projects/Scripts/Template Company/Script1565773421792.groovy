import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.tritonNAT)

WebUI.click(findTestObject('Template for Company/Page_Home/a_Templates                Templates'))

WebUI.click(findTestObject('Template for Company/Page_Templates/a_Add New            Add New'))

WebUI.setText(findTestObject('Template for Company/Page_Templates/input_Name_WorkflowName'), TempName)

WebUI.selectOptionByValue(findTestObject('Template for Company/Page_Templates/select_DealCompanyContact'), '2', true)

WebUI.selectOptionByValue(findTestObject('Template for Company/Page_Templates/AMD - Armenian Dram'), 'ALL', true)

WebUI.setText(findTestObject('Template for Company/Page_Templates/textarea_Description_WorkflowDescription'), TempDesc)

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Template for Company/Page_Templates/input_Description_btn btn-primary'))

WebUI.click(findTestObject('Template for Company/Page_Company Template/a_Add Task'))

WebUI.setText(findTestObject('Template for Company/Page_Company Template/input_Name_WorkflowTaskName'), TaskName)

WebUI.setText(findTestObject('Template for Company/Page_Company Template/textarea_Description_WorkflowTaskDescription'), 
    TaskDesc)

WebUI.setText(findTestObject('Template for Company/Page_Company Template/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    TaskAssignedTo)

WebUI.click(findTestObject('Template for Company/Page_Company Template/input_Completion Date_btn btn-primary'))

WebUI.click(findTestObject('Template for Company/Page_Company Template/a_Company Page'))

WebUI.click(findTestObject('Template for Company/Page_Company Page/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Template for Company/Page_Company Page/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Company/Page_Company Page/a_Company Template'))

WebUI.click(findTestObject('Template for Company/Page_Company Template/a_Create New Workflow                    Create New Workflow'))

WebUI.setText(findTestObject('Template for Company/Page_Company Template/input_Name_WorkflowName'), WorkflowName)

WebUI.selectOptionByValue(findTestObject('Template for Company/Page_Company Template/ALL - Albanian Lek'), 'AZN', true)

WebUI.setText(findTestObject('Template for Company/Page_Company Template/textarea_Description_WorkflowDescription'), WorkflowDesc)

WebUI.click(findTestObject('Template for Company/Page_Company Template/input_Description_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Company/Page_Company Workflow/a_Add Task'))

WebUI.setText(findTestObject('Template for Company/Page_Company Workflow/input_Name_WorkflowTaskName'), WorkflowTaskName)

WebUI.setText(findTestObject('Template for Company/Page_Company Workflow/textarea_Description_WorkflowTaskDescription'), 
    WorkflowTaskDesc)

WebUI.setText(findTestObject('Template for Company/Page_Company Workflow/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    WorkflowTaskAssignedTo)

WebUI.click(findTestObject('Template for Company/Page_Company Workflow/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Template for Company/Page_Company Workflow/a_Document Notification'))

WebUI.click(findTestObject('Template for Company/Page_Document Notification/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Template for Company/Page_Document Notification/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Company/Page_Document Notification/a_Home'))

WebUI.closeBrowser()

