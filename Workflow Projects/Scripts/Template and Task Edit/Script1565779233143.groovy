import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

//Launch URL
WebUI.navigateToUrl(GlobalVariable.tritonNAT)

//click Templates Button
WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_Home/a_Templates                Templates'))

//
WebUI.setText(findTestObject('Object Repository/Edit Temp,Task/Page_Templates/input_Templates_searchText'), SearchName)

WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_Templates/button_Filter            Filter'))

WebUI.waitForPageLoad(5)

WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_Templates/a_ksr Template'))

WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_ksr Template/a_Edit                Edit'))

WebUI.setText(findTestObject('Object Repository/Edit Temp,Task/Page_ksr Template/input_Name_WorkflowName'), TempName)

WebUI.setText(findTestObject('Object Repository/Edit Temp,Task/Page_ksr Template/textarea_Template Creation'), TempDesc)

WebUI.click(findTestObject('Edit Temp,Task/Page_ksr Template/input_Template Creation_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Edit Temp,Task/Page_ksr Template/a_Deal Page'))

WebUI.refresh()

WebUI.click(findTestObject('Edit Temp,Task/Page_Deal Page/a_Edit                Edit'))

WebUI.setText(findTestObject('Edit Temp,Task/Page_Deal Page/input_Name_WorkflowTaskName'), TaskName)

WebUI.setText(findTestObject('Edit Temp,Task/Page_Deal Page/textarea_Task Creation'), TaskDes)

WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_Deal Page/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/Edit Temp,Task/Page_Deal Page/a_ksr Template(Deal)'))

WebUI.closeBrowser()

