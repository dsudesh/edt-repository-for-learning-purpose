import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.tritonNAT)

WebUI.click(findTestObject('Template for Contact/Page_Home/a_Templates                Templates'))

WebUI.click(findTestObject('Template for Contact/Page_Templates/a_Add New            Add New'))

WebUI.setText(findTestObject('Template for Contact/Page_Templates/input_Name_WorkflowName'), Name)

WebUI.selectOptionByValue(findTestObject('Template for Contact/Page_Templates/select_DealCompanyContact'), '3', true)

WebUI.selectOptionByValue(findTestObject('Template for Contact/Page_Templates/BAM - Convertible Marks'), 'AUD', true)

WebUI.setText(findTestObject('Template for Contact/Page_Templates/textarea_Description_WorkflowDescription'), Desc)

WebUI.click(findTestObject('Template for Contact/Page_Templates/input_Description_btn btn-primary'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Template/a_Add Task'))

WebUI.setText(findTestObject('Template for Contact/Page_Contact Template/input_Name_WorkflowTaskName'), TaskName)

WebUI.setText(findTestObject('Template for Contact/Page_Contact Template/textarea_Description_WorkflowTaskDescription'), 
    TaskDesc)

WebUI.setText(findTestObject('Template for Contact/Page_Contact Template/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    TaskAssignedTo)

WebUI.click(findTestObject('Template for Contact/Page_Contact Template/input_Completion Date_btn btn-primary'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Template/a_Contact Page'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Page/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Page/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Contact/Page_Contact Page/a_Contact Template'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Template/a_Create New Workflow                    Create New Workflow'))

WebUI.setText(findTestObject('Template for Contact/Page_Contact Template/input_Name_WorkflowName'), WorkName)

WebUI.selectOptionByValue(findTestObject('Template for Contact/Page_Contact Template/AUD - Australian Dollar'), 'AUD', true)

WebUI.setText(findTestObject('Template for Contact/Page_Contact Template/textarea_Description_WorkflowDescription'), WorkDesc)

WebUI.click(findTestObject('Template for Contact/Page_Contact Template/input_Description_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Contact/Page_Contact Workflow/a_Add Task'))

WebUI.setText(findTestObject('Template for Contact/Page_Contact Workflow/input_Name_WorkflowTaskName'), WorkTaskName)

WebUI.setText(findTestObject('Template for Contact/Page_Contact Workflow/textarea_Description_WorkflowTaskDescription'), 
    WorkTaskDesc)

WebUI.setText(findTestObject('Template for Contact/Page_Contact Workflow/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    WorkTaskAssignedTo)

WebUI.click(findTestObject('Template for Contact/Page_Contact Workflow/input_Completion Date_btn btn-primary'))

WebUI.click(findTestObject('Template for Contact/Page_Contact Workflow/a_Add contact'))

WebUI.click(findTestObject('Template for Contact/Page_Add contact/button_Start Progress                        Start Progress'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/Template for Contact/Page_Add contact/a_Home'))

WebUI.closeBrowser()

