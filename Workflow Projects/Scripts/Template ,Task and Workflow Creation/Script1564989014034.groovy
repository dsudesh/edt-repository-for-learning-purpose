import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://triton.debtx.com/wkf/')

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Home/a_Templates                Templates'))

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Templates/span_Add New'))

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Templates/input_Name_WorkflowName'), Templatename)

WebUI.selectOptionByValue(findTestObject('Object Repository/Create Temp Task Work/Page_Templates/Currency'), 'BHD', true)

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Templates/textarea_Description_WorkflowDescription'), 
    TemplateDescription)

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Templates/input_Description_btn btn-primary'))

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/a_Add Task'))

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/input_Name_WorkflowTaskName'), Taskname)

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/textarea_Description_WorkflowTaskDescription'), 
    TaskDescription)

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    Assigned)

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/input_Completion Date_btn btn-primary'))

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/a_Create New Workflow                    Create New Workflow'))

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/input_Name_WorkflowName'), WorkflowName)

WebUI.selectOptionByValue(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/Currency 1'), 'BND', true)

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/textarea_Description_WorkflowDescription'), 
    WorkflowDescription)

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Temp/input_Description_btn btn-primary'))

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Work flow for Rest Temp/a_Rest Task1'))

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Task1/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Task1/input_Completion Date_btn btn-primary'))

WebUI.refresh()

WebUI.setText(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Task1/textarea_History_CommentCommentText'), 
    Comments)

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Rest Task1/input_History_btn btn-primary'))

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('https://triton.debtx.com/wkf/')

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/Create Temp Task Work/Page_Home/span_Workflows'))

WebUI.refresh()

WebUI.closeBrowser()

