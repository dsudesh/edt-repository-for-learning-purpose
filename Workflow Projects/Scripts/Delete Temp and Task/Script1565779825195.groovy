import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

//Launch URL
WebUI.navigateToUrl(GlobalVariable.tritonNAT)

//click Template Button
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_Home/a_Templates                Templates'))

//Template search
WebUI.setText(findTestObject('Object Repository/Delete Temp and Task/Page_Templates/input_Templates_searchText'), SearchTemp)

//click filter
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_Templates/button_Filter            Filter'))

WebUI.waitForPageLoad(5)

//click template
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_Templates/a_ksr Template(Deal)'))

//click Task
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_ksr Template(Deal)/a_Task Deal Page'))

WebUI.waitForPageLoad(5)

//refresh
WebUI.refresh()

//click delete > Task
WebUI.click(findTestObject('Delete Temp and Task/Page_Task Deal Page/a_Delete                Delete'))

//confirm delete
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_Task Deal Page/input_Task Deal Page_btn btn-primary'))

//click delete > Template
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_ksr Template(Deal)/a_Delete                Delete'))

//confrim delete
WebUI.click(findTestObject('Object Repository/Delete Temp and Task/Page_ksr Template(Deal)/input_ksr Template(Deal)_btn btn-primary'))

//close
WebUI.closeBrowser()

