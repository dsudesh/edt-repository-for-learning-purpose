import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.tritonNAT)

WebUI.click(findTestObject('Template for Deal/Page_Home/a_Templates                Templates'))

WebUI.click(findTestObject('Template for Deal/Page_Templates/a_Add New            Add New'))

WebUI.setText(findTestObject('Template for Deal/Page_Templates/input_Name_WorkflowName'), TempName)

WebUI.selectOptionByValue(findTestObject('Template for Deal/Page_Templates/select_DealCompanyContact'), '1', true)

WebUI.selectOptionByValue(findTestObject('Template for Deal/Page_Templates/AED - UAE Dirham'), 'AFN', true)

WebUI.setText(findTestObject('Template for Deal/Page_Templates/textarea_Description_WorkflowDescription'), TempDesc)

WebUI.click(findTestObject('Template for Deal/Page_Templates/input_Description_btn btn-primary'))

WebUI.click(findTestObject('Template for Deal/Page_Template/a_Add Task'))

WebUI.setText(findTestObject('Template for Deal/Page_Template/input_Name_WorkflowTaskName'), TaskName)

WebUI.setText(findTestObject('Template for Deal/Page_Template/textarea_Description_WorkflowTaskDescription'), TaskDesc)

WebUI.setText(findTestObject('Template for Deal/Page_Template/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    TaskAssignedTo)

WebUI.click(findTestObject('Template for Deal/Page_Template/input_Completion Date_btn btn-primary'))

WebUI.click(findTestObject('Template for Deal/Page_Template/a_Deal Page'))

WebUI.click(findTestObject('Template for Deal/Page_Deal Page/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Template for Deal/Page_Deal Page/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Deal/Page_Deal Page/a_Template'))

WebUI.click(findTestObject('Template for Deal/Page_Template/a_Create New Workflow                    Create New Workflow'))

WebUI.setText(findTestObject('Template for Deal/Page_Template/input_Name_WorkflowName'), WorkflowName)

WebUI.setText(findTestObject('Template for Deal/Page_Template/textarea_Description_WorkflowDescription'), WorkflowDesc)

WebUI.click(findTestObject('Template for Deal/Page_Template/input_Description_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Deal/Page_Workflow/a_Add Task'))

WebUI.setText(findTestObject('Template for Deal/Page_Workflow/input_Name_WorkflowTaskName'), WorkflowTaskName)

WebUI.setText(findTestObject('Template for Deal/Page_Workflow/textarea_Description_WorkflowTaskDescription'), WorkflowTaskDesc)

WebUI.setText(findTestObject('Template for Deal/Page_Workflow/input_Assigned To_WorkflowTaskTaskLevelFields1TextValue'), 
    WorkflowTaskAssignedTo)

WebUI.click(findTestObject('Template for Deal/Page_Workflow/input_Completion Date_btn btn-primary'))

WebUI.click(findTestObject('Template for Deal/Page_Workflow/a_Notification'))

WebUI.click(findTestObject('Template for Deal/Page_Notification/a_Mark Complete                    Mark Complete'))

WebUI.click(findTestObject('Template for Deal/Page_Notification/input_Completion Date_btn btn-primary'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Deal/Page_Notification/a_Workflow'))

WebUI.waitForPageLoad(5)

WebUI.refresh()

WebUI.click(findTestObject('Template for Deal/Page_Workflow/a_Home'))

WebUI.closeBrowser()

